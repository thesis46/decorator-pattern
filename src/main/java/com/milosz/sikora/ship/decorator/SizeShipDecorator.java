package com.milosz.sikora.ship.decorator;


import com.milosz.sikora.ship.Ship;

public abstract class SizeShipDecorator implements Ship {
    protected Ship decoratedShip;

    protected SizeShipDecorator(Ship decoratedShip) {
        this.decoratedShip = decoratedShip;
    }
}
