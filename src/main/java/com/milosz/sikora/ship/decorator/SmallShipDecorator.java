package com.milosz.sikora.ship.decorator;

import com.milosz.sikora.ship.Ship;

public class SmallShipDecorator extends SizeShipDecorator {
    public SmallShipDecorator(Ship decoratedShip) {
        super(decoratedShip);
    }

    @Override
    public void move() {
        decoratedShip.move();
        System.out.println("I move quickly, I'm small.");
    }
}
