package com.milosz.sikora.ship.decorator;

import com.milosz.sikora.ship.Ship;


public abstract class ArmouredShipDecorator implements Ship {
    protected Ship decoratedShip;
    protected int survivableTimeOfShelling;
    protected ArmouredShipDecorator(Ship decoratedShip, int survivableTimeOfShelling) {
        this.decoratedShip = decoratedShip;
        this.survivableTimeOfShelling = survivableTimeOfShelling;
    }

}
