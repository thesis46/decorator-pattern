package com.milosz.sikora.ship.decorator;

import com.milosz.sikora.ship.Ship;

import java.time.LocalDateTime;

public class HeavyArmouredShipDecorator extends ArmouredShipDecorator {
    public HeavyArmouredShipDecorator(Ship decoratedShip) {
        super(decoratedShip, 50);
    }

    @Override
    public void move() {
        System.out.println("I'm not armoured, Im vulnerable");
        var start = LocalDateTime.now();
        System.out.printf("They have started shooting at me at %s, but dont worry i will survive %ds", start, survivableTimeOfShelling);
        decoratedShip.move();
    }
}
