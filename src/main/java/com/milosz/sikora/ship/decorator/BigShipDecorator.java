package com.milosz.sikora.ship.decorator;


import com.milosz.sikora.ship.Ship;

public class BigShipDecorator extends SizeShipDecorator {
    public BigShipDecorator(Ship decoratedShip) {
        super(decoratedShip);
    }

    @Override
    public void move() {
        decoratedShip.move();
        System.out.println("I move slowly, I'm big.");
    }
}
