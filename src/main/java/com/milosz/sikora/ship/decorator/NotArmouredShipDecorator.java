package com.milosz.sikora.ship.decorator;

import com.milosz.sikora.ship.Ship;

import java.time.LocalDateTime;

public class NotArmouredShipDecorator extends ArmouredShipDecorator {
    public NotArmouredShipDecorator(Ship decoratedShip) {
        super(decoratedShip, 10);
    }

    @Override
    public void move() {
        System.out.println("I'm not armoured, Im vulnerable");
        var start = LocalDateTime.now();
        System.out.printf("They have started shooting at me at %s, I wont survive long, just %ds", start, this.survivableTimeOfShelling);
        decoratedShip.move();
    }
}
