package com.milosz.sikora.ship;

public class HospitalShip implements Ship{
    @Override
    public void move() {
        System.out.println("I'm healing people");
    }
}
