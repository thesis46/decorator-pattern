package com.milosz.sikora;

import com.milosz.sikora.ship.FishShip;
import com.milosz.sikora.ship.HospitalShip;
import com.milosz.sikora.ship.Ship;
import com.milosz.sikora.ship.Warship;
import com.milosz.sikora.ship.decorator.BigShipDecorator;
import com.milosz.sikora.ship.decorator.HeavyArmouredShipDecorator;
import com.milosz.sikora.ship.decorator.NotArmouredShipDecorator;
import com.milosz.sikora.ship.decorator.SmallShipDecorator;

public class Main {

    public static void main(String[] args) {

        Ship bigArmouredHospitalShip = new HeavyArmouredShipDecorator(new BigShipDecorator(new HospitalShip()));
        Ship bigNotArmouredFishShip = new NotArmouredShipDecorator(new BigShipDecorator(new FishShip()));
        Ship bigArmouredWarship = new HeavyArmouredShipDecorator(new BigShipDecorator(new Warship()));
        Ship bigNotArmouredHospitalShip = new NotArmouredShipDecorator(new BigShipDecorator((new HospitalShip())));
        Ship justWarship = new Warship();
        Ship smallArmouredWarship = new SmallShipDecorator(new HeavyArmouredShipDecorator(new Warship()));


        bigArmouredHospitalShip.move();
        bigNotArmouredFishShip.move();
        bigArmouredWarship.move();
        bigNotArmouredHospitalShip.move();
        smallArmouredWarship.move();
        justWarship.move();
    }
}
